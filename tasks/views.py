from django.views.generic.edit import CreateView
from tasks.models import Task
from django.urls import reverse_lazy


# Create your views here.


class TaskCreateView(CreateView):
    model = Task
    template_name = "tasks/new.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    success_url = reverse_lazy("show_project")
